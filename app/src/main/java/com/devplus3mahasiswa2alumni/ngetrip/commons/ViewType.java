package com.devplus3mahasiswa2alumni.ngetrip.commons;

/**
 * Created by Pance on 15/02/2017.
 */

public interface ViewType {
    int getViewType();
}
