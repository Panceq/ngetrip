package com.devplus3mahasiswa2alumni.ngetrip;

import android.os.Bundle;

import com.devplus3mahasiswa2alumni.ngetrip.fragment.MyTripFragment;

import butterknife.ButterKnife;

public class MyTripActivity extends BaseDrawerActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_trip);
        ButterKnife.bind(this);

        setToolbarTitle("My Trip");

        noFab(true);

        MyTripFragment fragment = new MyTripFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container_mytrip, fragment).commit();
    }
}
