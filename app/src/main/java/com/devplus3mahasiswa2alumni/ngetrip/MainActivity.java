package com.devplus3mahasiswa2alumni.ngetrip;

import android.*;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;

import com.devplus3mahasiswa2alumni.ngetrip.adapter.ViewPagerAdapter;
import com.devplus3mahasiswa2alumni.ngetrip.fragment.AllPostFragment;
import com.devplus3mahasiswa2alumni.ngetrip.fragment.TripFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseDrawerActivity {


    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.tabs)
    TabLayout tabs;

    private static final int READ_EXTERNAL_STORAGE_PERMISSION = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setupViewPager(viewpager);
        tabs.setupWithViewPager(viewpager);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            requestPermission();
        }

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addViewPagerFragment(new AllPostFragment(), "All Post");
        viewPagerAdapter.addViewPagerFragment(new TripFragment(), "Trip");
        viewPager.setAdapter(viewPagerAdapter);
    }

    private void requestPermission(){
        if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, READ_EXTERNAL_STORAGE_PERMISSION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case READ_EXTERNAL_STORAGE_PERMISSION:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){

                }else{
                    toasShort("Permission denied, some of function may not working properly");
                }
                break;
        }
    }
}
