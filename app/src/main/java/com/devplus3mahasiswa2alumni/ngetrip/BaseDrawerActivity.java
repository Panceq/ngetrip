package com.devplus3mahasiswa2alumni.ngetrip;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.design.internal.NavigationMenu;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import io.github.yavski.fabspeeddial.FabSpeedDial;
import io.github.yavski.fabspeeddial.FabSpeedDialBehaviour;

public class BaseDrawerActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    public Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private FabSpeedDial fabSpeedDial;
    private CircleImageView imgProfile;
    private Menu menuDrawer;

    private TextView tvLogout, tvUsername, tvEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected boolean useToolbar(){
        return true;
    }

    protected void noFab(boolean status){
        if(fabSpeedDial!=null){
            if(status){
                fabSpeedDial.setVisibility(View.GONE);
            }
        }
    }

    protected void setToolbarTitle(String title){
        View toolbarView = getSupportActionBar().getCustomView();
        TextView tvTitle = (TextView) toolbarView.findViewById(R.id.toolbar_title);
        tvTitle.setText(title);
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        DrawerLayout fullView = (DrawerLayout)getLayoutInflater().inflate(R.layout.activity_base_drawer, null);
        FrameLayout activityContainer = (FrameLayout) fullView.findViewById(R.id.frame);
        getLayoutInflater().inflate(layoutResID, activityContainer, true);
        super.setContentView(fullView);

        NavigationView navigationView = (NavigationView)fullView.findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
        menuDrawer = navigationView.getMenu();
        toolbar = (Toolbar)fullView.findViewById(R.id.toolbar);
        imgProfile = (CircleImageView)navigationView.getHeaderView(0).findViewById(R.id.img_profile);
        tvLogout = (TextView)navigationView.getHeaderView(0).findViewById(R.id.logout);
        tvUsername = (TextView)navigationView.getHeaderView(0).findViewById(R.id.tv_username_login);
        tvEmail = (TextView)navigationView.getHeaderView(0).findViewById(R.id.tv_email_login);

        if(useToolbar()){
            setSupportActionBar(toolbar);

            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setCustomView(R.layout.action_bar);

            this.getSupportActionBar().setElevation(0);
        }else{
            toolbar.setVisibility(View.GONE);
        }

        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
            }
        });

        tvUsername.setText(getUsernameLogin());
        tvEmail.setText(getEmailLogin());

        tvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPreferences(context, userId, "0");

                String getUserId = getPreferences(context, userId);
                if(getUserId.equals("0")){
                    startActivity(new Intent(context, SignInActivity.class));
                }else{
                    toasShort("Something went wrong, can not log out.");
                }
            }
        });

        fabSpeedDial = (FabSpeedDial)findViewById(R.id.fab_speed_dial);
        fabSpeedDial.setMenuListener(new FabSpeedDial.MenuListener() {
            @Override
            public boolean onPrepareMenu(NavigationMenu navigationMenu) {
                return true;
            }

            @Override
            public boolean onMenuItemSelected(MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id){
                    case R.id.menu_create_post:
                        startActivity(new Intent(getApplicationContext(), CreatePostActivity.class));
                        break;
                    case R.id.menu_create_trip:
                        startActivity(new Intent(getApplicationContext(), CreateTripActivity.class));
                        break;
                }

                return true;
            }

            @Override
            public void onMenuClosed() {

            }
        });


        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);


        //initializing navigation
        View view = getSupportActionBar().getCustomView();
        ImageView imgMenu = (ImageView) view.findViewById(R.id.img_menu);
        imgMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(Gravity.LEFT);
            }
        });

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.nav_home:
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                break;
            case R.id.nav_about:
                toasShort("test");
                break;
            case R.id.nav_mytrip:
                startActivity(new Intent(getApplicationContext(), MyTripActivity.class));
                break;
        }
        return true;
    }
}
