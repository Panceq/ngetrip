package com.devplus3mahasiswa2alumni.ngetrip.model;

import java.util.List;

/**
 * Created by Pance on 2/27/17.
 */

public class ImageResponse {
    public boolean error;
    public List<Image> images;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }
}
