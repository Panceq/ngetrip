package com.devplus3mahasiswa2alumni.ngetrip.model;


/**
 * Created by Pance on 25/02/2017.
 */

public class LoginResponse{

    public boolean error;

    public int id;

    public String username;
    public String profile_picture;
    public String email;
    public String status;
    public String login_using;

    public LoginResponse() {
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLogin_using() {
        return login_using;
    }

    public void setLogin_using(String login_using) {
        this.login_using = login_using;
    }
}
