package com.devplus3mahasiswa2alumni.ngetrip.model;

import com.devplus3mahasiswa2alumni.ngetrip.DisplayableItem;

/**
 * Created by Pance on 23/02/2017.
 */

public class Trip implements DisplayableItem{

    public String id;
    public String user_id;
    public String title;
    public String description;
    public String origin;
    public String going_to;
    public String start_time;
    public String finish_time;
    public String number_of_people;
    public String age;
    public String sex;
    public String created_at;

    //user info
    public String username;
    public String image;
    public String email;


    public Trip(String title, String description, String origin, String going_to, String start_time, String finish_time, String number_of_people, String age, String sex, String created_at) {
        this.title = title;
        this.description = description;
        this.origin = origin;
        this.going_to = going_to;
        this.start_time = start_time;
        this.finish_time = finish_time;
        this.number_of_people = number_of_people;
        this.age = age;
        this.sex = sex;
        this.created_at = created_at;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getGoing_to() {
        return going_to;
    }

    public void setGoing_to(String going_to) {
        this.going_to = going_to;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getFinish_time() {
        return finish_time;
    }

    public void setFinish_time(String finish_time) {
        this.finish_time = finish_time;
    }

    public String getNumber_of_people() {
        return number_of_people;
    }

    public void setNumber_of_people(String number_of_people) {
        this.number_of_people = number_of_people;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
