package com.devplus3mahasiswa2alumni.ngetrip.model;

import java.util.List;

/**
 * Created by Pance on 2/27/17.
 */

public class PostResponse {

    public boolean error;
    public List<Post> post;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<Post> getPost() {
        return post;
    }

    public void setPost(List<Post> post) {
        this.post = post;
    }
}
