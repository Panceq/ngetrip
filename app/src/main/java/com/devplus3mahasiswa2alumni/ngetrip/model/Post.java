package com.devplus3mahasiswa2alumni.ngetrip.model;

import com.devplus3mahasiswa2alumni.ngetrip.DisplayableItem;

/**
 * Created by Pance on 2/27/17.
 */

public class Post implements DisplayableItem{
    public String id;
    public String user_id;
    public String content;
    public String created_at;


    //user info
    public String username;
    public String image;
    public String email;

    //delete after api alrady hosting or re-configure this model
    public int image_post;

    public Post(String content, int image_post, String created_at) {
        this.content = content;
        this.image_post = image_post;
        this.created_at = created_at;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getImage_post() {
        return image_post;
    }

    public void setImage_post(int image_post) {
        this.image_post = image_post;
    }
}
