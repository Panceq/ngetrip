package com.devplus3mahasiswa2alumni.ngetrip.model;

/**
 * Created by Pance on 25/02/2017.
 */

public class GeneralAPIResponse {
    public boolean error;
    public String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
