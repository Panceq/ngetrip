package com.devplus3mahasiswa2alumni.ngetrip.model;

import java.util.List;

/**
 * Created by Pance on 23/02/2017.
 */

public class TripResponse {

    public boolean error;
    public List<Trip> trips;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<Trip> getTrip() {
        return trips;
    }

    public void setTrip(List<Trip> trip) {
        this.trips = trip;
    }


}
