package com.devplus3mahasiswa2alumni.ngetrip;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.devplus3mahasiswa2alumni.ngetrip.model.GeneralAPIResponse;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateTripActivity extends BaseDrawerActivity {

    @BindView(R.id.et_from)
    EditText etFrom;
    @BindView(R.id.et_going_to)
    EditText etGoingTo;
    @BindView(R.id.et_start_time_trip)
    EditText etStartTimeTrip;
    @BindView(R.id.et_finish_time)
    EditText etFinishTimeTrip;
    @BindView(R.id.img_down)
    ImageView imgDown;
    @BindView(R.id.tv_countofpeople)
    TextView tvCountofpeople;
    @BindView(R.id.img_up)
    ImageView imgUp;
    @BindView(R.id.img_count_of_people_trip)
    ImageView imgCountPeople;
    @BindView(R.id.et_age)
    EditText etAge;
    @BindView(R.id.spin_sex)
    Spinner spinSex;
    @BindView(R.id.et_title_trip)
    EditText etTitleTrip;
    @BindView(R.id.et_desc_trip)
    EditText etDescTrip;
    @BindView(R.id.sv_trip)
    ScrollView svTrip;
    @BindView(R.id.btn_create_trip)
    Button btnCreateTrip;

    private List<String> spinGender = new ArrayList<>();
    private int count = 0;

    private String from, goingTo, startTime, finishTime, countOfPeople, age, gender, title, desc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_trip);
        ButterKnife.bind(this);

        //set title bar
        setToolbarTitle("Create Trip");

        // no use floating button
        noFab(true);

        //set up gender/sex spinnner
        setUpSpinner();


        imgDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (count == 0) {
                    tvCountofpeople.setText("0");
                } else {
                    count -= 1;
                    tvCountofpeople.setText("" + count);
                }
            }
        });

        imgUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count += 1;
                tvCountofpeople.setText("" + count);
            }
        });

        setUpStartDate();
        setUpFinishDate();
    }

    private void setUpSpinner() {
        spinGender.add("Gender");
        spinGender.add("Male");
        spinGender.add("Female");
        spinGender.add("Male/Female");

        ArrayAdapter arrayAdapter = new ArrayAdapter(CreateTripActivity.this, android.R.layout.simple_list_item_1, spinGender);
        spinSex.setAdapter(arrayAdapter);
    }


    private void setUpStartDate() {
        etStartTimeTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);

                Dialog dialog = new DatePickerDialog(CreateTripActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                        SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd");
                        GregorianCalendar d = new GregorianCalendar(i, i1, i2);

                        Calendar cl = Calendar.getInstance();
                        cl.setTimeInMillis(d.getTime().getTime());

                        etStartTimeTrip.setText(simpledateformat.format(cl.getTime()));

                    }
                }, year, month, day);

                dialog.show();

            }
        });

    }


    private void setUpFinishDate() {
        etFinishTimeTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);

                Dialog dialog = new DatePickerDialog(CreateTripActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                        SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd");
                        GregorianCalendar d = new GregorianCalendar(i, i1, i2);

                        Calendar cl = Calendar.getInstance();
                        cl.setTimeInMillis(d.getTime().getTime());

                        etFinishTimeTrip.setText(simpledateformat.format(cl.getTime()));

                    }
                }, year, month, day);

                dialog.show();

            }
        });

    }

    @OnClick(R.id.btn_create_trip)
    public void onClick() {
        if(checkForm()){
            createTrip();
        }else {
            toasShort("Fill the blank data");
        }
    }

    private boolean checkForm(){
        from = etFrom.getText().toString();
        goingTo = etGoingTo.getText().toString();
        startTime = etStartTimeTrip.getText().toString();
        finishTime = etFinishTimeTrip.getText().toString();
        countOfPeople = tvCountofpeople.getText().toString();
        age = etAge.getText().toString();
        gender = spinSex.getSelectedItem().toString();
        title = etTitleTrip.getText().toString();
        desc = etDescTrip.getText().toString();

        if(from.isEmpty() || goingTo.isEmpty() || startTime.isEmpty() || finishTime.isEmpty() || countOfPeople.isEmpty() || age.isEmpty() || gender.isEmpty() || title.isEmpty() || desc.isEmpty()){
            return false;
        }else
            return true;
    }

    private void createTrip(){
        showLoadingDialog();
        String uid = getUserId();
        Call<GeneralAPIResponse> call = ngetripAPI.createTrip(uid, title, desc, from, goingTo, startTime, finishTime, countOfPeople, age, gender);
        call.enqueue(new Callback<GeneralAPIResponse>() {
            @Override
            public void onResponse(Call<GeneralAPIResponse> call, Response<GeneralAPIResponse> response) {
                dismisLoadingDialog();
                if(response.isSuccessful()){
                    startActivity(new Intent(CreateTripActivity.this, MainActivity.class));
                }else {
                    toasShort("Failed to create trip, try again :)");
                }
            }

            @Override
            public void onFailure(Call<GeneralAPIResponse> call, Throwable t) {
                dismisLoadingDialog();
                toasShort(t.getMessage());
            }
        });
    }
}
