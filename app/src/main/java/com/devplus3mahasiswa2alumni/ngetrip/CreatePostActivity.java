package com.devplus3mahasiswa2alumni.ngetrip;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.devplus3mahasiswa2alumni.ngetrip.model.GeneralAPIResponse;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreatePostActivity extends BaseDrawerActivity {

    @BindView(R.id.et_create_post)
    EditText etCreatePost;
    @BindView(R.id.btn_addpicture)
    Button btnAddpicture;
    @BindView(R.id.img_post_preview)
    ImageView imgPostPreview;
    @BindView(R.id.btn_create_post)
    Button btnCreatePost;

    private ByteArrayOutputStream byteArrayOutputStream;
    private Bitmap bitmapOriginal;
    private String imgToStingBase64;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_post);
        ButterKnife.bind(this);

        setToolbarTitle("Create Post");

        noFab(true);

        byteArrayOutputStream = new ByteArrayOutputStream();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK && data.getData() != null) {
            Uri uri = data.getData();
            try {
                bitmapOriginal = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                imgPostPreview.setImageBitmap(bitmapOriginal);
            } catch (IOException e) {

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick({R.id.btn_addpicture, R.id.btn_create_post})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_addpicture:
                pickImage();
                break;
            case R.id.btn_create_post:
                String content = etCreatePost.getText().toString();
                try{
                    bitmapOriginal.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                    byte[] imgByte = byteArrayOutputStream.toByteArray();
                    imgToStingBase64 = Base64.encodeToString(imgByte, Base64.DEFAULT);
                }catch (NullPointerException e){

                }
                if(content.isEmpty() || imgToStingBase64.isEmpty()){
                    toasShort("Content and Image can not be blank");
                }else{
                    createPost(content, imgToStingBase64);
                }
                break;
        }
    }

    private void createPost(String content, String image){
        showLoadingDialog();
        String userId = getUserId();
        String imageName = generateRandomString();
        Call<GeneralAPIResponse> call = ngetripAPI.createPost(userId, content, image, imageName);
        call.enqueue(new Callback<GeneralAPIResponse>() {
            @Override
            public void onResponse(Call<GeneralAPIResponse> call, Response<GeneralAPIResponse> response) {
                dismisLoadingDialog();
                if(response.isSuccessful()){
                    startActivity(new Intent(context, MainActivity.class));
                }else {
                    toasShort("Failed create post");
                }
            }

            @Override
            public void onFailure(Call<GeneralAPIResponse> call, Throwable t) {
                dismisLoadingDialog();
                toasShort(t.getMessage());
            }
        });
    }
}
