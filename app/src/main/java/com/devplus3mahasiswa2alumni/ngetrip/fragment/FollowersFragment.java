package com.devplus3mahasiswa2alumni.ngetrip.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.devplus3mahasiswa2alumni.ngetrip.R;

/**
 * Created by Pance on 08/03/2017.
 */

public class FollowersFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_followers, container, false);

        return view;
    }
}
