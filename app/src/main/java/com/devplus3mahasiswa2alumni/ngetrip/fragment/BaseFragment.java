package com.devplus3mahasiswa2alumni.ngetrip.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.devplus3mahasiswa2alumni.ngetrip.api.NgeTripAPIService;
import com.devplus3mahasiswa2alumni.ngetrip.api.NgetripAPI;

/**
 * Created by Pance on 2/27/17.
 */

public class BaseFragment extends Fragment {

    public ProgressDialog dialog;
    protected NgeTripAPIService ngeTripAPIService;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ngeTripAPIService = NgetripAPI.getClient().create(NgeTripAPIService.class);
    }

    public void showLoadingDialog(){
        if(dialog==null || !dialog.isShowing())
            dialog = ProgressDialog.show(getContext(),"Loading","Please wait...");
    }
    public void dismisLoadingDialog(){
        if(dialog!=null && dialog.isShowing())
            try {
                dialog.dismiss();
            } catch (Exception e) {
                //e.printStackTrace();
            }

    }
}
