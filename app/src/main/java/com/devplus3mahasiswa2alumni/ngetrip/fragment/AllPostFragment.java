package com.devplus3mahasiswa2alumni.ngetrip.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.devplus3mahasiswa2alumni.ngetrip.BaseActivity;
import com.devplus3mahasiswa2alumni.ngetrip.DisplayableItem;
import com.devplus3mahasiswa2alumni.ngetrip.R;
import com.devplus3mahasiswa2alumni.ngetrip.adapter.MainAdapter;
import com.devplus3mahasiswa2alumni.ngetrip.api.NgeTripAPIService;
import com.devplus3mahasiswa2alumni.ngetrip.api.NgetripAPI;
import com.devplus3mahasiswa2alumni.ngetrip.model.Post;
import com.devplus3mahasiswa2alumni.ngetrip.model.PostResponse;
import com.devplus3mahasiswa2alumni.ngetrip.model.Trip;
import com.devplus3mahasiswa2alumni.ngetrip.model.TripResponse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Pance on 2/27/17.
 */

public class AllPostFragment extends BaseFragment {

    @BindView(R.id.rv_allpost)
    RecyclerView rvAllpost;

    private boolean isTripLoaded, isPostLoaded;

    private List<DisplayableItem> allPost = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_allpost, container, false);
        ButterKnife.bind(this, view);

        rvAllpost.setLayoutManager(new LinearLayoutManager(getActivity()));
        MainAdapter adapter = new MainAdapter(getActivity(), getAllPost());
        rvAllpost.setAdapter(adapter);
//        getPost();
        return view;
    }


    //dummy data
    private List<DisplayableItem> getAllPost(){
        List<DisplayableItem> allpost = new ArrayList<>();

        allpost.add(new Post("Content 1", R.drawable.labuan_cermin, "an hour ago"));
        allpost.add(new Post("Content 2", R.drawable.nusa_dua_bali, "12 minutes ago"));
        allpost.add(new Post("Content 3", R.drawable.pulau_maratua, "a minute ago"));
        allpost.add(new Trip("Title 1", "Desc 1", "Jakarta", "Nusa Dua, Bali", "23 Maret 2017", "30 Maret 2017", "5", "20-30", "Male/Female", " 2 hours ago"));
        allpost.add(new Trip("Title 2", "Desc 2", "Jakarta", "Labuan Cermin, Kalimantan", "1 April 2017", "3 April 2017", "3", "23-25", "Male/Female", " 10 hours ago"));

        Collections.shuffle(allpost);
        return allpost;
    }

//    private void displayAllPost() {
//
//        Collections.shuffle(allPost);
//        MainAdapter adapter = new MainAdapter(getActivity(), allPost);
//        rvAllpost.setAdapter(adapter);
//    }
//
//    private void getPost() {
//        showLoadingDialog();
//        allPost.clear();
//        Call<PostResponse> call = ngeTripAPIService.getPost();
//        call.enqueue(new Callback<PostResponse>() {
//            @Override
//            public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
//                if (response.isSuccessful()) {
//                    PostResponse postResponse = response.body();
//                    for (int i = 0; i < postResponse.getPost().size(); i++) {
//                        allPost.add(postResponse.getPost().get(i));
//                    }
//                    isPostLoaded = true;
//                    getTrip();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<PostResponse> call, Throwable t) {
//                dismisLoadingDialog();
//            }
//        });
//    }
//
//    private void getTrip() {
//        if (isPostLoaded) {
//            Call<TripResponse> call = ngeTripAPIService.getTrip();
//            call.enqueue(new Callback<TripResponse>() {
//                @Override
//                public void onResponse(Call<TripResponse> call, Response<TripResponse> response) {
//                    dismisLoadingDialog();
//                    if (response.isSuccessful()) {
//                        TripResponse tripResponse = response.body();
//                        for (int i = 0; i < tripResponse.getTrip().size(); i++) {
//                            allPost.add(tripResponse.getTrip().get(i));
//
//                        }
//                        isTripLoaded = true;
//                        displayAllPost();
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<TripResponse> call, Throwable t) {
//                    dismisLoadingDialog();
//
//                }
//            });
//        }
//    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
