package com.devplus3mahasiswa2alumni.ngetrip.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.devplus3mahasiswa2alumni.ngetrip.BaseActivity;
import com.devplus3mahasiswa2alumni.ngetrip.DisplayableItem;
import com.devplus3mahasiswa2alumni.ngetrip.R;
import com.devplus3mahasiswa2alumni.ngetrip.adapter.MainAdapter;
import com.devplus3mahasiswa2alumni.ngetrip.model.TripResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Pance on 08/03/2017.
 */

public class MyTripFragment extends BaseFragment {

    @BindView(R.id.rv_mytrip)
    RecyclerView rvMytrip;

    private List<DisplayableItem> tripList = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mytrip, container, false);
        ButterKnife.bind(this, view);

        rvMytrip.setLayoutManager(new LinearLayoutManager(getContext()));
        getMyTrip();
        return view;
    }

    private void getMyTrip() {
        tripList.clear();
        String userId = BaseActivity.getPreferences(getContext(), BaseActivity.userId);
        Call<TripResponse> call = ngeTripAPIService.getTripByUserId(userId);
        call.enqueue(new Callback<TripResponse>() {
            @Override
            public void onResponse(Call<TripResponse> call, Response<TripResponse> response) {
                if (response.isSuccessful()) {
                    TripResponse tripResponse = response.body();
                    for(int i=0; i<tripResponse.getTrip().size(); i++){
                        tripList.add(tripResponse.getTrip().get(i));
                    }

                    MainAdapter adapter = new MainAdapter(getActivity(), tripList);
                    rvMytrip.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<TripResponse> call, Throwable t) {

            }
        });
    }
}
