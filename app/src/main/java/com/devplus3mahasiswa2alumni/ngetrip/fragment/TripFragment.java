package com.devplus3mahasiswa2alumni.ngetrip.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.devplus3mahasiswa2alumni.ngetrip.DisplayableItem;
import com.devplus3mahasiswa2alumni.ngetrip.R;
import com.devplus3mahasiswa2alumni.ngetrip.adapter.MainAdapter;
import com.devplus3mahasiswa2alumni.ngetrip.api.NgeTripAPIService;
import com.devplus3mahasiswa2alumni.ngetrip.api.NgetripAPI;
import com.devplus3mahasiswa2alumni.ngetrip.model.Trip;
import com.devplus3mahasiswa2alumni.ngetrip.model.TripResponse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Pance on 2/27/17.
 */

public class TripFragment extends BaseFragment {

    @BindView(R.id.rv_trip)
    RecyclerView rvTrip;

    private NgeTripAPIService ngeTripAPIService;

    private List<DisplayableItem> tripList = new ArrayList<>();
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ngeTripAPIService = NgetripAPI.getClient().create(NgeTripAPIService.class);

        getTrip();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_trip, container, false);
        ButterKnife.bind(this, view);

        rvTrip.setLayoutManager(new LinearLayoutManager(getActivity()));

        return view;
    }

//    //dummy data
//    private List<DisplayableItem> getTrip() {
//        List<DisplayableItem> allpost = new ArrayList<>();
//
//        allpost.add(new Trip("Title 1", "Desc 1", "Jakarta", "Nusa Dua, Bali", "23 Maret 2017", "30 Maret 2017", "5", "20-30", "Male/Female", " 2 hours ago"));
//        allpost.add(new Trip("Title 2", "Desc 2", "Jakarta", "Labuan Cermin, Kalimantan", "1 April 2017", "3 April 2017", "3", "23-25", "Male/Female", " 10 hours ago"));
//
//        return allpost;
//    }

    private void getTrip() {
        Call<TripResponse> call = ngeTripAPIService.getTrip();
        call.enqueue(new Callback<TripResponse>() {
            @Override
            public void onResponse(Call<TripResponse> call, Response<TripResponse> response) {
                dismisLoadingDialog();
                if (response.isSuccessful()) {
                    TripResponse tripResponse = response.body();
                    for (int i = 0; i < tripResponse.getTrip().size(); i++) {
                        tripList.add(tripResponse.getTrip().get(i));

                        }
                        displayTrip();
                    }
                }

            @Override
            public void onFailure(Call<TripResponse> call, Throwable t) {
                dismisLoadingDialog();

            }
        });
    }

    private void displayTrip() {

        MainAdapter adapter = new MainAdapter(getActivity(), tripList);
        rvTrip.setAdapter(adapter);
    }
}
