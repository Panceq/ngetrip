package com.devplus3mahasiswa2alumni.ngetrip;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.devplus3mahasiswa2alumni.ngetrip.model.GeneralAPIResponse;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends BaseActivity {

    @BindView(R.id.et_username_reg)
    EditText etUsernameReg;
    @BindView(R.id.et_email_reg)
    EditText etEmailReg;
    @BindView(R.id.et_password_reg)
    EditText etPasswordReg;
    @BindView(R.id.img_profile_reg)
    ImageView imgProfileReg;
    @BindView(R.id.btn_reg)
    Button btnReg;

    private String username, email, password;
    private Bitmap bitmapImgProfile, bitmapOriginal;
    private BitmapDrawable bitmapDrawable;
    private ByteArrayOutputStream out;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        out = new ByteArrayOutputStream();
    }


    private void prepareReg() {
        username = etUsernameReg.getText().toString();
        email = etEmailReg.getText().toString();
        password = etPasswordReg.getText().toString();


        String imageName = generateRandomString();

        //encode bitmap image profile to string base64
        try{
            bitmapOriginal.compress(Bitmap.CompressFormat.JPEG, 100, out);
        }catch (NullPointerException e){
            bitmapDrawable = (BitmapDrawable) imgProfileReg.getDrawable();
            bitmapOriginal = bitmapDrawable.getBitmap();
        }

        byte[] imgByte = out.toByteArray();
        String imageEncoded = Base64.encodeToString(imgByte, Base64.DEFAULT);

        if (checkForm()) {
            register(email, username, password, imageEncoded, imageName);
        } else {
            toasShort("Username, email, password can not be blank");
        }
    }

    private boolean checkForm() {
        if (username.isEmpty() || email.isEmpty() || password.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    private void register(String email, String username, String password, String bitmapImgProfile, String imageName) {
        showLoadingDialog();
        Call<GeneralAPIResponse> call = ngetripAPI.register(username, email, password, bitmapImgProfile, imageName);
        call.enqueue(new Callback<GeneralAPIResponse>() {
            @Override
            public void onResponse(Call<GeneralAPIResponse> call, Response<GeneralAPIResponse> response) {
                dismisLoadingDialog();
                if (response.isSuccessful()) {
                    GeneralAPIResponse apiResponse = response.body();

                    if(!apiResponse.isError()){
                        toasShort(apiResponse.getMessage());
                        startActivity(new Intent(context, SignInActivity.class));
                    }
                }else{
                    toasShort("Email not valid");
                }
            }

            @Override
            public void onFailure(Call<GeneralAPIResponse> call, Throwable t) {
                dismisLoadingDialog();
                toasShort(t.getMessage());
            }
        });
    }


    @OnClick({R.id.img_profile_reg, R.id.btn_reg})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_profile_reg:
                pickImage();
                break;
            case R.id.btn_reg:
                prepareReg();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1 && resultCode == RESULT_OK && data.getData() != null){
            Uri uri = data.getData();
            try{
                bitmapOriginal = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);

                imgProfileReg.setImageBitmap(bitmapOriginal);
            }catch (IOException e){

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


}
