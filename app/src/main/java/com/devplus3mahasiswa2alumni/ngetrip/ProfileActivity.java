package com.devplus3mahasiswa2alumni.ngetrip;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.devplus3mahasiswa2alumni.ngetrip.adapter.ViewPagerAdapter;
import com.devplus3mahasiswa2alumni.ngetrip.fragment.FollowersFragment;
import com.devplus3mahasiswa2alumni.ngetrip.fragment.FollowingFragment;
import com.devplus3mahasiswa2alumni.ngetrip.fragment.MyPostFragment;
import com.devplus3mahasiswa2alumni.ngetrip.fragment.MyTripFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends BaseDrawerActivity {

    @BindView(R.id.img_profile)
    CircleImageView imgProfile;
    @BindView(R.id.display_name)
    TextView displayName;
    @BindView(R.id.bio_profile)
    TextView bioProfile;
    @BindView(R.id.rl_profile)
    RelativeLayout rlProfile;
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.activity_profile)
    RelativeLayout activityProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        setToolbarTitle("Profile");
        noFab(true);

        setUpViewPager(viewpager);
        tabs.setupWithViewPager(viewpager);
    }


    private void setUpViewPager(ViewPager viewpager){
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addViewPagerFragment(new MyPostFragment(), "My Post");
        viewPagerAdapter.addViewPagerFragment(new MyTripFragment(), "My Trip");
        viewPagerAdapter.addViewPagerFragment(new FollowersFragment(), "Followers");
        viewPagerAdapter.addViewPagerFragment(new FollowingFragment(), "Following");
        viewpager.setAdapter(viewPagerAdapter);

    }
}
