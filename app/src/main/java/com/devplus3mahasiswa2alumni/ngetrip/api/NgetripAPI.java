package com.devplus3mahasiswa2alumni.ngetrip.api;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Pance on 23/02/2017.
 */

public class NgetripAPI {

    public static final String BASE_URL = "http://192.168.100.26/ntp_rest_api/";
    public  static final String BASE_URL_POST_IMAGE = "http://192.168.100.26/ntp_rest_api/image_post/";
    public  static final String BASE_URL_PROFILE_IMAGE = "http://192.168.100.26/ntp_rest_api/image_profile/";

    private static Retrofit retrofit = null;

    public static Retrofit getClient(){
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();

        if(retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(okHttpClient)
                    .build();
        }

        return retrofit;
    }
}
