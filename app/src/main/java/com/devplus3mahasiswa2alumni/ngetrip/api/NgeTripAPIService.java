package com.devplus3mahasiswa2alumni.ngetrip.api;

import com.devplus3mahasiswa2alumni.ngetrip.model.GeneralAPIResponse;
import com.devplus3mahasiswa2alumni.ngetrip.model.ImageResponse;
import com.devplus3mahasiswa2alumni.ngetrip.model.LoginResponse;
import com.devplus3mahasiswa2alumni.ngetrip.model.PostResponse;
import com.devplus3mahasiswa2alumni.ngetrip.model.TripResponse;

import io.reactivex.Observable;
import io.reactivex.Observer;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Pance on 23/02/2017.
 */

public interface NgeTripAPIService {

    @GET("trip")
    Call<TripResponse> getTrip();

    @FormUrlEncoded
    @POST("register")
    Call<GeneralAPIResponse> register(@Field("username") String username,
                                         @Field("email") String email,
                                         @Field("password") String password,
                                         @Field("image") String image,
                                         @Field("imageName") String imageName);

    @FormUrlEncoded
    @POST("login")
    Observable<LoginResponse> login(@Field("email") String email,
                                    @Field("password") String password);

    @GET("post")
    Call<PostResponse> getPost();


    @FormUrlEncoded
    @POST("insert_join_trip")
    Observable<GeneralAPIResponse> insertJoinTrip(@Field("trip_id") String trip_id,
                                                @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("create_post")
    Call<GeneralAPIResponse> createPost(@Field("user_id") String user_id,
                                        @Field("content") String content,
                                        @Field("image") String image,
                                        @Field("image_name") String image_name);
    @FormUrlEncoded
    @POST("create_trip")
    Call<GeneralAPIResponse> createTrip(@Field("user_id") String user_id,
                                        @Field("title") String title,
                                        @Field("description") String description,
                                        @Field("origin") String origin,
                                        @Field("going_to") String going_to,
                                        @Field("start_time") String start_time,
                                        @Field("finish_time") String finish_time,
                                        @Field("number_of_people") String number_of_people,
                                        @Field("age") String age,
                                        @Field("sex") String sex);

    @FormUrlEncoded
    @POST("trip_by_user")
    Call<TripResponse> getTripByUserId(@Field("user_id") String user_id);


}
