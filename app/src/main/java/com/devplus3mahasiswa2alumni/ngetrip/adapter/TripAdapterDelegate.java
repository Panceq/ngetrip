package com.devplus3mahasiswa2alumni.ngetrip.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.devplus3mahasiswa2alumni.ngetrip.BaseActivity;
import com.devplus3mahasiswa2alumni.ngetrip.DisplayableItem;
import com.devplus3mahasiswa2alumni.ngetrip.R;
import com.devplus3mahasiswa2alumni.ngetrip.api.NgeTripAPIService;
import com.devplus3mahasiswa2alumni.ngetrip.api.NgetripAPI;
import com.devplus3mahasiswa2alumni.ngetrip.model.GeneralAPIResponse;
import com.devplus3mahasiswa2alumni.ngetrip.model.Trip;
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Pance on 3/7/17.
 */

public class TripAdapterDelegate extends AdapterDelegate<List<DisplayableItem>> {



    private LayoutInflater inflater;
    private CompositeDisposable disposable;
    public static NgeTripAPIService ngetripAPI;

    public TripAdapterDelegate(Activity activity) {
        inflater = activity.getLayoutInflater();
        disposable = new CompositeDisposable();
        ngetripAPI = NgetripAPI.getClient().create(NgeTripAPIService.class);
    }

    @Override
    protected boolean isForViewType(@NonNull List<DisplayableItem> items, int position) {
        return items.get(position) instanceof Trip;
    }

    @NonNull
    @Override
    protected RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new TripViewHolder(inflater.inflate(R.layout.trip_item, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull List<DisplayableItem> items, int position, @NonNull RecyclerView.ViewHolder holder, @NonNull List<Object> payloads) {
        TripViewHolder vh = (TripViewHolder) holder;
        Trip trip = (Trip) items.get(position);
        vh.bind(trip);
    }

     class TripViewHolder extends RecyclerView.ViewHolder {

//        @BindView(R.id.img_profile_trip)
//        CircleImageView imgProfileTrip;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.tv_username_trip)
        TextView tvUsername;
        @BindView(R.id.tv_from_trip)
        TextView tvFromTrip;
        @BindView(R.id.tv_going_to_trip)
        TextView tvGoingToTrip;
        @BindView(R.id.btn_jointrip)
        Button btnJointrip;
        @BindView(R.id.btn_chat_trip)
        Button btnChatTrip;
        @BindView(R.id.tv_date_from)
        TextView tvDateFrom;
        @BindView(R.id.tv_date_until)
        TextView tvDateUntil;
        @BindView(R.id.tv_count_of_people_trip)
        TextView tvCountOfPeopleTrip;
        @BindView(R.id.tv_sex_trip)
        TextView tvSexTrip;
        @BindView(R.id.tv_age_trip)
        TextView tvAgeTrip;
        @BindView(R.id.tv_count_of_people_already_join)
        TextView tvCountOfPeopleAlreadyJoin;
        @BindView(R.id.tv_trip_desc)
        TextView tvTripDesc;
        @BindView(R.id.img_like_trip)
        ImageView imgLikeTrip;
        @BindView(R.id.likes_count_trip)
        TextView likesCountTrip;
        @BindView(R.id.img_comment_trip)
        ImageView imgCommentTrip;
        @BindView(R.id.comments_count_trip)
        TextView commentsCountTrip;
        @BindView(R.id.img_share_trip)
        ImageView imgShareTrip;
        @BindView(R.id.main_layout_trip)
        FrameLayout mainLayoutTrip;
        @BindView(R.id.created_at)
        TextView createdAt;

        Random random;
        Context context;

        public TripViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            random = new Random();
            context = itemView.getContext();
        }

        public void bind(Trip trip) {
            final String tripId = trip.getId();
            String userIDTrip = trip.getUser_id();
            final String userId = BaseActivity.getPreferences(context, BaseActivity.userId);
            if(userId.equals(userIDTrip)){
                btnJointrip.setVisibility(View.GONE);
                btnChatTrip.setVisibility(View.GONE);
            }

            btnJointrip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addToJoinTrip(tripId, context, userId);
                }
            });
//            try{
//                Picasso.with(itemView.getContext()).load(NgetripAPI.BASE_URL_PROFILE_IMAGE+trip.getImage()).into(imgProfileTrip);
//            }catch (IllegalArgumentException e){
//                e.printStackTrace();
//            }
            tvUsername.setText(trip.getUsername());
            mainLayoutTrip.setBackgroundColor(Color.rgb(random.nextInt(256), random.nextInt(256), random.nextInt(256)));
            tvTitle.setText(trip.getTitle());
            tvFromTrip.setText(trip.getOrigin());
            tvGoingToTrip.setText(trip.getGoing_to());
            tvDateFrom.setText(trip.getStart_time());
            tvDateUntil.setText(trip.getFinish_time());
            tvCountOfPeopleTrip.setText(trip.getNumber_of_people());
            tvSexTrip.setText(trip.getSex());
            tvAgeTrip.setText(trip.getAge());
            tvTripDesc.setText(trip.getDescription());
            createdAt.setText(trip.getCreated_at());
        }
    }

    private void addToJoinTrip(String tripId, final Context context, String userId){
        disposable.add(ngetripAPI.insertJoinTrip(tripId, userId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
        .subscribeWith(new DisposableObserver<GeneralAPIResponse>() {
            @Override
            public void onNext(GeneralAPIResponse value) {
                Toast.makeText(context,""+value.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(Throwable e) {
                Log.e("mimiJoin","Error");
            }

            @Override
            public void onComplete() {
                Log.e("mimiJoin","Complete");
            }
        }));
    }


    @Override
    protected void onViewDetachedFromWindow(RecyclerView.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        disposable.clear();
    }
}
