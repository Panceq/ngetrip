package com.devplus3mahasiswa2alumni.ngetrip.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.devplus3mahasiswa2alumni.ngetrip.DisplayableItem;
import com.hannesdorfmann.adapterdelegates3.AdapterDelegatesManager;

import java.util.List;

/**
 * Created by Pance on 3/7/17.
 */

public class MainAdapter extends RecyclerView.Adapter {

    private AdapterDelegatesManager<List<DisplayableItem>> delegatesManager;
    private List<DisplayableItem> items;

    public MainAdapter(Activity activity, List<DisplayableItem> items) {
        this.items = items;

        //delegates
        delegatesManager = new AdapterDelegatesManager<>();
        delegatesManager.addDelegate(new PostAdapterDelegate(activity));
        delegatesManager.addDelegate(new TripAdapterDelegate(activity));

    }

    @Override
    public int getItemViewType(int position) {
        return delegatesManager.getItemViewType(items, position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return delegatesManager.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        delegatesManager.onBindViewHolder(items, position, holder);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position, List payloads) {
        delegatesManager.onBindViewHolder(items, position, holder, payloads);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
