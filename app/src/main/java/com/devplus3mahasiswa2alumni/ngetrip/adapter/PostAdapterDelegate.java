package com.devplus3mahasiswa2alumni.ngetrip.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.devplus3mahasiswa2alumni.ngetrip.BaseActivity;
import com.devplus3mahasiswa2alumni.ngetrip.DisplayableItem;
import com.devplus3mahasiswa2alumni.ngetrip.R;
import com.devplus3mahasiswa2alumni.ngetrip.api.NgetripAPI;
import com.devplus3mahasiswa2alumni.ngetrip.model.Post;
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Pance on 3/7/17.
 */

public class PostAdapterDelegate extends AdapterDelegate<List<DisplayableItem>> {


    private LayoutInflater inflater;

    public PostAdapterDelegate(Activity activity) {
        inflater = activity.getLayoutInflater();
    }

    @Override
    protected boolean isForViewType(@NonNull List<DisplayableItem> items, int position) {
        return items.get(position) instanceof Post;
    }

    @NonNull
    @Override
    protected RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new PostViewHolder(inflater.inflate(R.layout.post_item, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull List<DisplayableItem> items, int position, @NonNull RecyclerView.ViewHolder holder, @NonNull List<Object> payloads) {
        PostViewHolder vh = (PostViewHolder) holder;
        Post post = (Post)items.get(position);
        vh.bind(post);

    }

    static class PostViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_profile_post)
        CircleImageView imgProfilePost;
        @BindView(R.id.tv_post_description)
        TextView tvPostDescription;
        @BindView(R.id.tv_username_post)
        TextView tvUsername;
        @BindView(R.id.likes_count)
        TextView likesCount;
        @BindView(R.id.comments_count)
        TextView commentsCount;
        @BindView(R.id.created_at)
        TextView createdAt;
        @BindView(R.id.img_post)
        ImageView imgPost;

        public PostViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(Post post){
            try{
                Picasso.with(itemView.getContext()).load(NgetripAPI.BASE_URL_PROFILE_IMAGE+post.getImage()).into(imgProfilePost);
            }catch (IllegalArgumentException e){
                e.printStackTrace();
            }
            tvUsername.setText(post.getUsername());
            tvPostDescription.setText(post.getContent());
            likesCount.setText("12,2k likes");
            commentsCount.setText("2,1k comments");
            createdAt.setText(post.getCreated_at());
            Picasso.with(itemView.getContext()).load(NgetripAPI.BASE_URL_POST_IMAGE+post.getImage_post()).into(imgPost);
        }
    }
}
