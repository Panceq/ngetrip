package com.devplus3mahasiswa2alumni.ngetrip;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.devplus3mahasiswa2alumni.ngetrip.api.NgeTripAPIService;
import com.devplus3mahasiswa2alumni.ngetrip.api.NgetripAPI;

import java.security.SecureRandom;

import butterknife.ButterKnife;

/**
 * Created by Pance on 15/02/2017.
 */

public class BaseActivity extends AppCompatActivity {

    public static String userId = "uid";
    public static String usernameLogin = "username";
    public static String emailLogin = "email";
    public static String imageLogin = "image";

    // API initialization
    public static NgeTripAPIService ngetripAPI;
    // application context
    Context context;

    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static SecureRandom rnd = new SecureRandom();

    public ProgressDialog dialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = getApplicationContext();

        ngetripAPI = NgetripAPI.getClient().create(NgeTripAPIService.class);
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        bindViews();
    }

    public void toasShort(String message){
        Toast.makeText(BaseActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    public static void setPreferences(Context context, String key, String value){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        pref.edit().putString(key, value).apply();
    }

    public static String getPreferences(Context context, String key){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        return pref.getString(key,"");
    }

    protected void bindViews(){
        ButterKnife.bind(this);
    }

    protected String generateRandomString(){
        int length = 20;
        StringBuilder sb = new StringBuilder(length);
        for( int i = 0; i < length; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return sb.toString();
    }

    protected void pickImage(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
    }

    public void showLoadingDialog(){
        if(dialog==null || !dialog.isShowing())
            dialog = ProgressDialog.show(this,"Loading","Please wait...");
    }
    public void dismisLoadingDialog(){
        if(dialog!=null && dialog.isShowing())
            try {
                dialog.dismiss();
            } catch (Exception e) {
                //e.printStackTrace();
            }

    }

    protected String getUserId(){
        String returnUserId = getPreferences(context, userId);
        return returnUserId;
    }

    public String getUsernameLogin(){
        String userName = getPreferences(context, usernameLogin);
        return userName;
    }

    public String getEmailLogin(){
        String email = getPreferences(context, emailLogin);
        return email;
    }

    public String getImageLogin(){
        String image = getPreferences(context, imageLogin);
        return image;
    }
}
