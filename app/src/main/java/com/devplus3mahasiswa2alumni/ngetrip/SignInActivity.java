package com.devplus3mahasiswa2alumni.ngetrip;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.devplus3mahasiswa2alumni.ngetrip.model.LoginResponse;
import com.devplus3mahasiswa2alumni.ngetrip.model.Trip;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class SignInActivity extends BaseActivity {

    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.fb_signin)
    ImageView fbSignin;
    @BindView(R.id.google_signin)
    ImageView googleSignin;

    public static final String TAG = SignInActivity.class.getSimpleName();

    //Rx
    private CompositeDisposable disposable;
    private List<Trip> tripList = new ArrayList<>();

    //Fb
    CallbackManager mCallbackManager;
    List<String> permissionNeeds = Arrays.asList("public_profile", "email", "user_friends");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);

        disposable = new CompositeDisposable();
        String isLogin = getPreferences(context, userId);

        //check if user is already login or not
        if (!isLogin.isEmpty() && !isLogin.equals("0")) {
            startActivity(new Intent(this, MainActivity.class));
        } else {
            // FB LOGIN
            fbSignin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AccessToken accessToken = AccessToken.getCurrentAccessToken();

                    if (accessToken != null) {
                        toasShort("Already Login");
                    } else {
                        mCallbackManager = CallbackManager.Factory.create();
                        LoginManager.getInstance().logInWithReadPermissions(SignInActivity.this, permissionNeeds);
                        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
                            @Override
                            public void onSuccess(LoginResult loginResult) {
                                Log.d(TAG, "FBLOGIN onSuccess");
                                if (!loginResult.getAccessToken().getToken().isEmpty()) {
                                    Log.e(TAG, "FBTOKEN: " + loginResult.getAccessToken());
                                    RequestData();
                                }
                            }

                            @Override
                            public void onCancel() {
                                Log.d(TAG, "FBLOGIN onCancel");
                            }

                            @Override
                            public void onError(FacebookException error) {
                                Log.d(TAG, "FBLOGIN error: " + error.getMessage());
                            }
                        });
                    }
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    //request data facebook
    public void RequestData() {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {

                JSONObject json = response.getJSONObject();
//                Profile login = response.
                try {
                    if (json != null) {

                        String text = "<b>Name :</b> " + json.getString("name") + "<br><br><b>Email :</b> " + json.getString("email") + "<br><br><b>Profile link :</b> " + json.getString("link");
                        Log.e(TAG, "" + json.getString("id").toString());
                        setPreferences(context, userId, json.getString("id").toString());
                        startActivity(new Intent(context, MainActivity.class));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @OnClick(R.id.btn_login)
    public void login(View v) {
        final String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();
        if (email.isEmpty() || password.isEmpty()) {
            toasShort("Email or Password can not be blank");
        } else {
            disposable.add(
                    ngetripAPI.login(email, password)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeWith(new DisposableObserver<LoginResponse>() {

                                @Override
                                public void onNext(LoginResponse value) {
                                    if (value.getId() != 0) {
                                        setPreferences(SignInActivity.this, userId, String.valueOf(value.getId()));
                                        setPreferences(SignInActivity.this, usernameLogin, value.getUsername());
                                        setPreferences(SignInActivity.this, emailLogin, value.getEmail());
                                        setPreferences(SignInActivity.this, imageLogin, value.getProfile_picture());
                                        startActivity(new Intent(context, MainActivity.class));
                                    } else {
                                        toasShort("Login failed. Incorrect credentials");
                                    }
                                }

                                @Override
                                public void onError(Throwable e) {
                                    toasShort("Login error, check your connection");
                                    Log.e(TAG, "Login error");
                                    e.printStackTrace();
                                }

                                @Override
                                public void onComplete() {
                                    Log.d(TAG, "Login complete");
                                }
                            })
            );
        }
    }

    @OnClick(R.id.btn_signup)
    public void onClick() {
        startActivity(new Intent(context, RegisterActivity.class));
    }
}
